FROM node:11.8.0-alpine

RUN npm install -g npm

WORKDIR /

COPY package.json package-lock.json /
RUN npm install

COPY . /

CMD [ "npm", "run", "test"]
